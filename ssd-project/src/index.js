import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import HomePage from './pages/home-page/app';
import SignInPage from './pages/sign-in-page/app';
import SignUpPage from './pages/sign-up-page/app';
import CandidateUser from './pages/users/candidate-user/app';
import ManagerUser from './pages/users/manager-user/app';
import UIStaffUser from './pages/users/ui-staff-user/app/UIStaffUser';
import TestEditor from './pages/tests-pages/test-editor/app/TestEditor';
import './index.css';
import AddModifyTest from "./pages/tests-pages/test-editor/app/AddModifyTest";
import TestModifier from "./pages/tests-pages/test-editor/app/TestModifier";
import ListOfCandidates from "./pages/users/manager-user/app/ListOfCandidates";
import PassTest from "./pages/tests-pages/candidate-test/app/PassTest";
import AssignInterview from './pages/users/manager-user/app/AssignInterview';

const getCookie = (cname) => {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

const Routing = () => {
    if (getCookie('auth_token') === '') {
        return (
            <Router>
                <Route exact path="/" component={HomePage} />
                <Route exact path="/home" component={HomePage} />
                <Route exact path="/registration" component={SignUpPage} />
                <Route exact path="/authorization" component={SignInPage} />
            </Router>
        );
    }
    else {
        return (
            <Router>
                <Route exact path="/" component={HomePage} />
                <Route exact path="/home" component={HomePage} />
                <Route exact path="/registration" component={SignUpPage} />
                <Route exact path="/authorization" component={SignInPage} />
                <Route exact path="/candidate-user" component={CandidateUser} />
                <Route exact path="/manager-user" component={ManagerUser} />
                <Route exact path="/ui-staff-user" component={UIStaffUser} />
                <Route exact path="/test-editor" component={TestEditor} />
                <Route exact path="/test-add-modify" component={AddModifyTest} />
                <Route exact path="/test-modify" component={TestModifier} />
                <Route exact path="/candidates-list" component={ListOfCandidates} />
                <Route exact path="/candidate-test" component={PassTest} />
                <Route exact path="/assign-interview" component={AssignInterview} />
            </Router>
        );
    }
}

ReactDOM.render(<Routing />, document.getElementById('root'));