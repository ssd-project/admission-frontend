import React, { Component } from 'react';
import ProfileHeader from "../../../users/candidate-user/components/ProfileHeader";
import '../../../users/profiles.css';
import Quiz from 'react-quiz-component';
export const quiz =  {
    "quizTitle": "React Quiz Component Demo",
    "quizSynopsis": "React knowledges test",
    "questions": [
        {
            "question": "How can you access the state of a component from inside of a member function?",
            "questionType": "text",
            "answers": [
                "this.getState()",
                "this.prototype.stateValue",
                "this.state",
                "this.values"
            ],
            "correctAnswer": "3"
        },
        {
            "question": "ReactJS is developed by _____?",
            "questionType": "text",
            "answers": [
                "Google Engineers",
                "Facebook Engineers"
            ],
            "correctAnswer": "2"
        }
    ]
}

const onCompleteAction = (obj) => {
    console.log(obj);
    return(
        <div className='list_text' >
            Number of questions: {obj.numberOfQuestions}
            <br/>
           Number of correct answers: {obj.numberOfCorrectAnswers}
        </div>

    )
}
class PassTest extends Component {

    render() {
        return (
            <div>
                <ProfileHeader username="Nadezhda" usersurname="Zagvozkina"/>
                <Quiz quiz={quiz} shuffle={true} showDefaultResult={false}  onComplete={onCompleteAction}/>
            </div>

        )
    }
}

PassTest.propTypes = {

}
export default PassTest;