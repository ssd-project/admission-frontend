import React from "react"
import ProfileHeader from "../../../users/manager-user/components/ProfileHeader";

export const quiz = {}

class TestEditor extends React.Component {
    constructor() {
        super();
        this.state = {
            quizTitle: "",
            quizSynopsis: "",
            questions: [{
                question: "",
                correctAnswer: "",
                answer_1: "",
                answer_2: "",
                answer_3: ""
            }],

        };
    }

    handleQuizTitleChange = evt => {
        this.setState({quizTitle: evt.target.value});
    };
    handleQuizSynopsisChange = evt => {
        this.setState({quizTitle: evt.target.value});
    };

    handleQuestionChange = idx => evt => {
        const newQuestions = this.state.questions.map((question, sidx) => {
            if (idx !== sidx) return question;
            return {...question, question: evt.target.value};
        });

        this.setState({questions: newQuestions});
    };
    handleAnswer1Change = idx => evt => {
        const newAnswers = this.state.questions.map((answer_1, sidx) => {
            if (idx !== sidx) return answer_1;
            return {...answer_1, answer_1: evt.target.value};
        });

        this.setState({questions: newAnswers});
    };
    handleAnswer2Change = idx => evt => {
        const newAnswers = this.state.questions.map((answer_2, sidx) => {
            if (idx !== sidx) return answer_2;
            return {...answer_2, answer_2: evt.target.value};
        });

        this.setState({questions: newAnswers});
    };
    handleAnswer3Change = idx => evt => {
        const newAnswers = this.state.questions.map((answer_3, sidx) => {
            if (idx !== sidx) return answer_3;
            return {...answer_3, answer_3: evt.target.value};
        });

        this.setState({questions: newAnswers});
    };
    handlecorrectAnswerChange = idx => evt => {
        const newAnswers = this.state.questions.map((correctAnswer, sidx) => {
            if (idx !== sidx) return correctAnswer;
            return {...correctAnswer, correctAnswer: evt.target.value};
        });

        this.setState({questions: newAnswers});
    };


    handleSubmit = evt => {
        const {quizTitle, questions} = this.state;
        alert(`Quiz: ${quizTitle} with ${questions.length} questions`);
    };

    handleAddQuestion = () => {
        this.setState({
            questions: this.state.questions.concat([{question: "", answers: [""]}])
        });
    };

    handleRemoveQuestion = idx => () => {
        this.setState({
            questions: this.state.questions.filter((s, sidx) => idx !== sidx)
        });
    };

    render() {
        return (
            <form  onSubmit={this.handleSubmit}>
                <ProfileHeader username="Nadezhda" usersurname="Zagvozkina"/>
                <br/>
                <input
                    type="text"
                    placeholder="quiz Name"
                    value={this.state.quizTitle}
                    onChange={this.handleQuizTitleChange}
                />
                <input
                    type="text"
                    placeholder="description"
                    value={this.state.quizSynopsis}
                    onChange={this.handleQuizSynopsisChange}
                />
                <h1>Questions</h1>

                {this.state.questions.map((question, idx) => (
                    <div className="question">
                        <input
                            type="text"
                            placeholder={`Question #${idx + 1}`}
                            value={question.question}
                            onChange={this.handleQuestionChange(idx)}
                        />
                        <input
                            type="text"
                            placeholder={`Correct Answer №`}
                            value={question.correctAnswer}
                            onChange={this.handlecorrectAnswerChange(idx)}
                        />
                            <div>
                                <input
                                    type="text"
                                    placeholder={`Answer # 1`}
                                    value={question.answer_1}
                                    onChange={this.handleAnswer1Change(idx)}
                                />
                                <input
                                    type="text"
                                    placeholder={`Answer #2`}
                                    value={question.answer_2}
                                    onChange={this.handleAnswer2Change(idx)}
                                />

                                <input
                                    type="text"
                                    placeholder={`Answer # 3`}
                                    value={question.answer_3}
                                    onChange={this.handleAnswer3Change(idx)}
                                />
                            </div>


                        <button
                            type="button"
                            onClick={this.handleRemoveQuestion(idx)}
                            className="signButtons"
                        >
                            -
                        </button>
                    </div>
                ))}
                <button
                    type="button"
                    onClick={this.handleAddQuestion}
                    className="signButtons"
                >
                    Add Question
                </button>
                <button className="signButtons">Submit</button>
            </form>
        );
    }
}

//const rootElement = document.getElementById("root");
export default TestEditor
