import React, { Component } from 'react';
import ProfileHeader from "../../../users/manager-user/components/ProfileHeader";
import { Link } from "react-router-dom";
import authenticate_user from '../../../authenticate_user';

class AddModifyTest extends Component {

    handleAddTestButton = async (event) => {
        event.preventDefault();
        const auth_response = await authenticate_user(this.getCookie('auth_token'));
        if (auth_response['anonymous'] === true) this.props.history.push('/authorization/');
        else if (auth_response['enrollee'] === true) this.props.history.push('/candidate-user/')
        else if (auth_response['staff'] === true) this.props.history.push('/ui-staff-user/')
        else if (auth_response['manager'] === true) this.props.history.push('/test-editor/')
        else this.props.history.push('/test-editor/');
    }

    handleModifyTestButton = async (event) => {
        event.preventDefault();
        const auth_response = await authenticate_user(this.getCookie('auth_token'));
        if (auth_response['anonymous'] === true) this.props.history.push('/authorization/');
        else if (auth_response['enrollee'] === true) this.props.history.push('/candidate-user/')
        else if (auth_response['staff'] === true) this.props.history.push('/ui-staff-user/')
        else if (auth_response['manager'] === true) this.props.history.push('/test-modify/')
        else this.props.history.push('/test-modify/');
    }

    render() {
        return (
            <div>
                <ProfileHeader username="Nadezhda" usersurname="Zagvozkina" />
                <div>
                    <button className="profile_buttons" type="button" onClick={this.handleAddTestButton}>
                        Add test
                    </button>
                    <button className="profile_buttons" type="button" onChange={this.handleModifyTestButton}>
                        Modify test
                    </button>
                </div>
            </div>

        )
    }
}

AddModifyTest.propTypes = {

}
export default AddModifyTest;