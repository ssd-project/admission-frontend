import React, { Component } from 'react';
import InputForm from '../components/InputForm';
import { Link, withRouter } from 'react-router-dom';
import '../../../index.css';
import { authoriseUser } from '../api-calls/api-calls';
import { timingSafeEqual } from 'crypto';
import authenticate_user from '../../authenticate_user';

class SignInPage extends Component {
    defaultState = {
        email: ['email', 'Email'],
        password: ['password', 'Password']
    }

    state = {
        email: ['email', 'Email'],
        password: ['password', 'Password'],

        error: false
    }

    handleInput = (stateValue, inputValue) => {
        let value;
        (inputValue === '') ?
            value = [stateValue, this.defaultState[stateValue][1]]
            :
            value = [stateValue, inputValue];

        this.setState({ [stateValue]: value });
    }

    setError = () => {
        this.setState({ error: true });
    }

    setToken = async (token) => {
        document.cookie = `auth_token=${token}`;
        const auth_response = await authenticate_user(token);
        console.log(auth_response);
        if (auth_response['anonymous'] === true) this.props.history.push('/authorization/');
        else if (auth_response['manager'] === true) this.props.history.push('/manager-user/')
        else if (auth_response['enrollee'] === true) this.props.history.push('/candidate-user/')
        else if (auth_response['staff'] === true) this.props.history.push('/ui-staff-user/')
        else this.props.history.push('/manager-user/');
    }

    handleAuthorizationButton = async (event) => {
        const data = this.state;
        authoriseUser(data, this.setToken, this.setError);
    }

    getCookie = (cname) => {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    render() {
        return (
            <div id="sign" className="mainPage">
                <header>
                    <Link to="/home">
                        <button className="signButtons" type="button"> Home </button>
                    </Link>
                </header>
                <body>
                    <div className="signBox">
                        <Link to="/authorization">
                            <button className="signButtons" type="button"> Sign In </button>
                        </Link>
                        <Link to="/registration">
                            <button className="signButtons" type="button">
                                Sign Up
                        </button>
                        </Link>
                        <InputForm placeholder={this.state.email} handleInput={this.handleInput} />
                        <InputForm placeholder={this.state.password} handleInput={this.handleInput} type={"password"} />
                        {
                            this.state.error ?
                                <p>Email or password are incorrect</p>
                                :
                                null
                        }
                        <button className="buttons" type="button" onClick={this.handleAuthorizationButton}>
                            Sign In
                        </button>
                    </div>
                </body>
                <div><br /><br /><br /><br /></div>
            </div>
        );
    }
}

export default SignInPage;