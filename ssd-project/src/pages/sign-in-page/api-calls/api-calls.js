const authoriseUser = async (state, setToken, setError) => {

    const { email, password } = state;
    const url = `http://inno-admission.live:8000/api/users/auth/`;
    await fetch(url,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: email[1],
                password: password[1]
            })
        })
        .then((response) => {
            if (!response.ok) {
                throw new Error(response.status);
            }
            else return response.json();
        })
        .then((data) => {
            setToken(data['token']);
            console.log('auth token: ', data['token']);
        })
        .catch((error) => {
            setError();
            console.log('error: ', error);
        });
}

export { authoriseUser };