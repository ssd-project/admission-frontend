import React from 'react';

const InputForm = (props) => {
    const handleInputChange = (event) => {
        props.handleInput(props.placeholder[0], event.target.value);
    }

    const getData = () => { return true; }

    return (
        <form>
            <input className="textForm" placeholder={props.placeholder[1]} type={props.type} onChange={handleInputChange} />
        </form>
    );
}

export default InputForm;