import React, { Component } from 'react';
import AccountInformation from '../components/AccountInformation';
import ProfileHeader from '../components/ProfileHeader';
import AvatarComponent from "../components/AvatarComponent";
import TestsAndDocumentation from "../components/TestsAndDocuments";
import { Link } from "react-router-dom";
import { verify_token } from '../api-calls/api-calls';
import authenticate_user from '../../../authenticate_user';

class CandidateUser extends Component {

    state = {
        token_verifiing_requested: false,
        token_declined: false,
        token_verified: false,
        user: null,
        data_is_set: false
    }

    getCookie = (cname) => {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    verifyToken = () => {
        const handleResponse = (response) => {
            if (response) this.setState({ token_verified: true });
            else this.setState({ token_declined: true });
        }
        verify_token(this.getCookie('auth_token'), handleResponse);
    }

    getUserInformation = () => {
        const url = `http://inno-admission.live:8000/api/users/profile/`;
        fetch(url, {
            method: 'GET',
            headers: { 'Authorization': "JWT " + this.getCookie('auth_token') }
        })
            .then(results => { return results.json(); })
            .then(data => {
                this.setState({ user: data, data_is_set: true });
            })
    };

    userInfo = () => { return this.state.user; }

    handleTestPassingButton = async (event) => {
        event.preventDefault();
        const auth_response = await authenticate_user(this.getCookie('auth_token'));
        if (auth_response['anonymous'] === true) this.props.history.push('/authorization/');
        else if (auth_response['enrollee'] === true) this.props.history.push('/candidate-test/')
        else if (auth_response['staff'] === true) this.props.history.push('/ui-staff-user/')
        else if (auth_response['manager'] === true) this.props.history.push('/manager-user/')
        else this.props.history.push('/candidate-test/');
    }

    render() {
        if (!this.state.token_verifiing_requested) {
            this.setState({ token_verifiing_requested: true });
            this.verifyToken();
        }

        if (this.state.token_verified) {
            if (!this.state.request_done) {
                this.getUserInformation();
                this.setState({ request_done: true })
            }
            return (
                <div>
                    <div>
                        <ProfileHeader
                            username={this.state.data_is_set ? this.userInfo().first_name : "undefined"}
                            usersurname={this.state.data_is_set ? this.userInfo().last_name : "undefined"} />
                        <AvatarComponent
                            usersurname={this.state.data_is_set ? this.userInfo().last_name : "undefined"}
                            username={this.state.data_is_set ? this.userInfo().first_name : "undefined"}
                            program={this.state.data_is_set ? this.userInfo().edu_program = 1 ? "Bachelor" : "Master" : "undefined"} />

                        <table align="center">
                            <tr>
                                <div className="profile_box" id="test_box">
                                    <a id="test_margin">You need to pass several tests before the interview</a>
                                    <button className="profile_buttons" type="button" onClick={this.handleTestPassingButton}>
                                        Get Now
                                    </button>
                                </div>
                            </tr>
                        </table>
                        <div>
                            <table align="center">
                                <tr>
                                    <td> <AccountInformation
                                        username={this.state.data_is_set ? this.userInfo().first_name : "undefined"}
                                        usersurname={this.state.data_is_set ? this.userInfo().last_name : "undefined"}
                                        emailAddress={this.state.data_is_set ? this.userInfo().email : "undefined"}
                                        birthday={this.state.data_is_set ? this.userInfo().birth_date : "undefined"}
                                    /></td>
                                    <td>
                                        <TestsAndDocumentation
                                            isPassport={true}
                                            isCertificate={false}
                                            getCookie={this.getCookie}
                                        />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            );
        }
        else if (this.state.token_declined) {
            this.props.history.push('/authorization');
        }
        else {
            return (
                <div className="lds-roller">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            )
        }
    }
}

CandidateUser.propTypes = {

}
export default CandidateUser;