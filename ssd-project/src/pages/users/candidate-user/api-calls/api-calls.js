const verify_token = (token, handleResponse) => {
    const url = `http://inno-admission.live:8000/api/users/api-token-verify/`;
    fetch(url,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ token: token })
        })
        .then((response) => {
            if (!response.ok) throw new Error(response.status);
            else return response.json();
        })
        .then((data) => { handleResponse(true); })
        .catch((error) => {
            handleResponse(false);
            console.log('error: ', error);
        });
}

const get_user_information = (token) => {

}

const upload_CV = (token, data) => {
    const url = `http://inno-admission.live:8000/api/users/documents/`;
    fetch(url,
        {
            method: 'POST',
            headers: {
                'Authorization': `JWT ${token}`
            },
            body: data
        })
        .then((response) => {
            if (!response.ok) throw new Error(response.status);
            else return response.json();
        })
        .then((data) => {
        })
        .catch((error) => {
            console.log('error: ', error);
        });
}


const upload_DIPLOMA = (token, data) => {
    const url = `http://inno-admission.live:8000/api/users/documents/`;
    fetch(url,
        {
            method: 'POST',
            headers: {
                'Authorization': `JWT ${token}`
            },
            body: JSON.stringify({
                "doc_type": 2,
                "file": data
            })
        })
        .then((response) => {
            if (!response.ok) throw new Error(response.status);
            else return response.json();
        })
        .then((data) => {
        })
        .catch((error) => {
            console.log('error: ', error);
        });
}


const upload_MOTIVATION_LETTER = (token, data) => {
    const url = `http://inno-admission.live:8000/api/users/documents/`;
    fetch(url,
        {
            method: 'POST',
            headers: {
                'Authorization': `JWT ${token}`
            },
            body: JSON.stringify({
                "doc_type": 3,
                "file": data
            })
        })
        .then((response) => {
            if (!response.ok) throw new Error(response.status);
            else return response.json();
        })
        .then((data) => {

        })
        .catch((error) => {
            console.log('error: ', error);
        });
}


const upload_RECOMENDATION = (token, data) => {
    const url = `http://inno-admission.live:8000/api/users/documents/`;
    fetch(url,
        {
            method: 'POST',
            headers: {
                'Authorization': `JWT ${token}`
            },
            body: JSON.stringify({
                "doc_type": 4,
                "file": data
            })
        })
        .then((response) => {
            if (!response.ok) throw new Error(response.status);
            else return response.json();
        })
        .then((data) => {
        })
        .catch((error) => {
            console.log('error: ', error);
        });
}


const upload_ID_SCAN = (token, data) => {
    const url = `http://inno-admission.live:8000/api/users/documents/`;
    fetch(url,
        {
            method: 'POST',
            headers: {
                'Authorization': `JWT ${token}`
            },
            body: JSON.stringify({
                "doc_type": 5,
                "file": data
            })
        })
        .then((response) => {
            if (!response.ok) throw new Error(response.status);
            else return response.json();
        })
        .then((data) => {
        })
        .catch((error) => {
            console.log('error: ', error);
        });
}

export {
    verify_token,
    get_user_information,
    upload_CV,
    upload_DIPLOMA,
    upload_MOTIVATION_LETTER,
    upload_RECOMENDATION,
    upload_ID_SCAN
}