import React from 'react';
import PropTypes from 'prop-types';
import { upload_CV, upload_DIPLOMA, upload_MOTIVATION_LETTER, upload_RECOMENDATION, upload_ID_SCAN } from '../api-calls/api-calls';
import FormData from 'form-data';

const TestsAndDocumentation = (props) => {
    let CV = undefined;
    let DIPLOMA = undefined;
    let MOTIVATION_LETTER = undefined;
    let RECOMENDATION = undefined;
    let ID_SCAN = undefined;

    const handleUploadCVButton = (event) => {
        event.preventDefault();
        const data = new FormData();
        data.append('doc_type', 1);
        data.append('file', event.target.files[0]);
        CV = data;
    }

    const handleUploadDIPLOMAButton = (event) => {
        event.preventDefault();
        const data = new FormData();
        data.append('doc_type', 2);
        data.append('file', event.target.files[0]);
        DIPLOMA = data;
    }

    const handleUploadMOTIVATION_LETTERButton = (event) => {
        event.preventDefault();
        const data = new FormData();
        data.append('doc_type', 3);
        data.append('file', event.target.files[0]);
        MOTIVATION_LETTER = data;
    }

    const handleUploadRECOMENDATIONButton = (event) => {
        event.preventDefault();
        const data = new FormData();
        data.append('doc_type', 4);
        data.append('file', event.target.files[0]);
        RECOMENDATION = data;
    }

    const handleUploadID_SCANButton = (event) => {
        event.preventDefault();
        const data = new FormData();
        data.append('doc_type', 5);
        data.append('file', event.target.files[0]);
        ID_SCAN = data;
    }
    
    const handleSaveFilesButton = (event) => {
        event.preventDefault();
        if (CV !== undefined) { 
            upload_CV(props.getCookie('auth_token'), CV); 
        }
        if (DIPLOMA !== undefined) { 
            upload_DIPLOMA(props.getCookie('auth_token'), DIPLOMA);
        }
        if (MOTIVATION_LETTER !== undefined) { 
            upload_MOTIVATION_LETTER(props.getCookie('auth_token'), MOTIVATION_LETTER);
        }
        if (RECOMENDATION !== undefined) { 
            upload_RECOMENDATION(props.getCookie('auth_token'), RECOMENDATION);
        }
        if (ID_SCAN !== undefined) {  
            upload_ID_SCAN(props.getCookie('auth_token'), ID_SCAN);
        }
        
    }

    return (
        <div>
            <div className="profile_box">
                <h2 id="profile_info_header">Test results</h2>
            </div>
            <div className="profile_box">
                <h2 id="profile_info_header">Uploaded documents</h2>
                <div className="image-upload">

                <label className="file" htmlFor="file-input">
                    CV
                </label>
                <input id="file-input" title="CV" type="file" onChange={handleUploadCVButton} />


                    <label className="file" htmlFor="file-input-1">
                        Diploma
                    </label>
                <input id="file-input-1" type="file" onChange={handleUploadDIPLOMAButton} />
                    <label className="file" htmlFor="file-input-2">
                        MotivationLetter
                    </label>
                <input id="file-input-2" type="file" onChange={handleUploadMOTIVATION_LETTERButton} />


                <br/><br/>
                    <label className="file" htmlFor="file-input-3">
                        Recomendation
                    </label>
                <input id="file-input-3" type="file" onChange={handleUploadRECOMENDATIONButton} />
                    <label className="file" htmlFor="file-input-4">
                        ID
                    </label>
                <input id="file-input-4" type="file" onChange={handleUploadID_SCANButton} />
                    <label className="file-save" htmlFor="save">
                        SAVE
                    </label>
                    <button id = "save" className="submit_btn" type="button" onClick={handleSaveFilesButton}>
                        <a id="plus">Save</a>
                    </button>

                </div>
            </div>
        </div>
    )
};



TestsAndDocumentation.propTypes = {
    isPassport: PropTypes.bool,
    isCertificate: PropTypes.bool
};


export default TestsAndDocumentation;
