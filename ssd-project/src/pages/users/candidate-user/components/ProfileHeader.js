import React from 'react';
import PropTypes from 'prop-types';
import Avatar from 'react-avatar';
import { Link } from 'react-router-dom';
import '../../../users/profiles.css';
const AccountInformation = (props) => {
    return (
        <div>
            <div className="profile_header">
                <Link to="">
                    <button className="log_out_btn" id = "mng_btn" type="button">Log out</button>
                </Link>
                <Link to="/candidate-user">
                <Avatar size="30px" name={props.username +" "+ props.usersurname} />
                </Link>
            </div>
            <div className="profile_header_buttons">
                <Link to="/candidate-user">
                    <button className="profile_buttons" type="button">Profile</button>
                </Link>

                <button className="profile_buttons" type="button">Tests</button>
            </div>
        </div>
    )
};

AccountInformation.propTypes = {
    username: PropTypes.string.isRequired,
    usersurname: PropTypes.string.isRequired
};


export default AccountInformation;
