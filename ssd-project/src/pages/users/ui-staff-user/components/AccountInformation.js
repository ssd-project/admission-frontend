import React from 'react';
import PropTypes from 'prop-types';
import Avatar from 'react-avatar';
const AccountInformation = (props) => {
        return (
            <div>
                <div className="profile_box">
                    <h2 id = "profile_info_header">Account</h2>
                    <a>Given Name <text id = "prof_info">{props.username}</text></a>
                    <br/>
                    <a>Family Name <text id = "prof_info">{props.usersurname}</text></a>
                    <br/>
                    <a>Birthday <text id = "prof_info">{props.birthday}</text></a>
                </div>
                <div className="profile_box">
                        <h2 id = "profile_info_header">Email</h2>
                       <a >Email address <br/>
                       <text id = "prof_info"> {props.emailAddress}</text></a>
                </div>
            </div>
        )
    };

    AccountInformation.propTypes = {
        username: PropTypes.string.isRequired,
        emailAddress: PropTypes.string.isRequired,
        usersurname:  PropTypes.string.isRequired,
        birthday: PropTypes.string.isRequired
    };


export default AccountInformation;
