import React, { Component } from 'react';
import AccountInformation from '../components/AccountInformation';
import ProfileHeader from '../components/ProfileHeader';
import AvatarComponent from "../components/AvatarComponent";
import AssignedInterviews from "../components/AssignedInterviews";

class UIStaffUser extends Component {

    render() {
        return (
            <div>
                <ProfileHeader username="Nadezhda" usersurname="Zagvozkina"/>

                <AvatarComponent usersurname="Zagvozkina" username="Nadezhda" degree="Professor"/>
                <div>
                    <table align="center">
                        <tr>
                            <td> <AccountInformation
                                username="Nadezhda"
                                usersurname="Zagvozkina"
                                emailAddress="nadya@whatever.com"
                                birthday="27.01.1998"
                            /></td>
                            <td>
                                <AssignedInterviews
                                />
                            </td>
                        </tr>
                    </table>
                </div>


            </div>
        )
    }
}

UIStaffUser.propTypes = {

}
export default UIStaffUser;