import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class AssignInterview extends Component {

    state = {
        candidatesListRequested: false,
        candidateListReceived: false,
        staffListRequested: false,
        staffListReceived: false,

        candidates_list: [],
        staff_list: [],

        staff_id: '',
        enrolee_id: ''
    }

    getCookie = (cname) => {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    getStaffList = () => {
        const url = 'http://inno-admission.live:8000/api/users/staff/';
        fetch(url,
            {
                method: 'GET',
                headers: {
                    'Authorization': `JWT ${this.getCookie('auth_token')}`,
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (!response.ok) throw new Error(response.status);
                else return response.json();
            })
            .then((data) => { 
                this.setState({ 
                    staff_list: data, 
                    staffListReceived: true, 
                    staff_id: data[0].id
                }) 
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }


    getCandidatesList = () => {
        const url = 'http://inno-admission.live:8000/api/users/';
        fetch(url,
            {
                method: 'GET',
                headers: {
                    'Authorization': `JWT ${this.getCookie('auth_token')}`,
                    'Content-Type': 'application/json'
                }
            })
            .then((response) => {
                if (!response.ok) throw new Error(response.status);
                else return response.json();
            })
            .then((data) => { 
                this.setState({ 
                    candidates_list: data, 
                    candidateListReceived: true,
                    enrolee_id: data[0].id
                }) 
            })
            .catch((error) => {
                console.log('error: ', error);
            });
    }

    onChangeStaff = (event) => {
        this.setState({ staff_id: event.target.value[0] });
        console.log(this.state.staff_id);
    }

    onChangeCandidate = (event) => {
        this.setState({ enrolee_id: event.target.value[0] });
        console.log(this.state.enrolee_id);
    }

    onClickAssignInterview = (event) => {
        event.preventDefault();
        const url = 'http://inno-admission.live:8000/api/users/interviews/';
        console.log('adf: ', this.state.staff_id, this.state.enrolee_id)
        // fetch(url,
        //     {
        //         method: 'POST',
        //         headers: {
        //             'Authorization': `JWT ${this.getCookie('auth_token')}`,
        //             'Content-Type': 'application/json'
        //         },
        //         body: {
        //             "staff_id": this.state.staff_id,
        //             "enrolee_id": this.state.enrolee_id
        //         }
        //     })
        //     .then((response) => {
        //         if (!response.ok) throw new Error(response.status);
        //         else return response.json();
        //     })
        //     .then((data) => { 
        //     })
        //     .catch((error) => {
        //         console.log('error: ', error);
        //     });
    }

    render() {
        if (!this.state.candidatesListRequested) {
            this.getCandidatesList();
            this.setState({ candidatesListRequested: true });
        }
        if (!this.state.staffListRequested) {
            this.getStaffList();
            this.setState({ staffListRequested: true });
        }
        return (
            <div>
                <Link to="/manager-user/">
                    <button>Back</button>
                </Link>
                <h1>Assign Interview page</h1>
                {
                    this.state.staffListReceived ?
                        <select id="selectDropdown" onChange={this.onChangeStaff}>
                            {
                                this.state.staff_list.map((item) => { return (<option>{item.id}: {item.name} {item.second_name}</option>) })
                            }
                        </select>
                        :
                        null
                }

                {
                    this.state.candidateListReceived ?
                        <select id="selectDropdown" onChange={this.onChangeCandidate}>
                            {
                                this.state.candidates_list.map((item) => { return (<option>{item.id}: {item.first_name} {item.last_name}</option>) })
                            }
                        </select>
                        :
                        null
                }
                <button onClick={this.onClickAssignInterview}>Assign Interview</button>
            </div>
        );
    }
}

export default AssignInterview;