import React, {Component} from 'react';

import ProfileHeader from '../components/ProfileHeader';
import '../../../users/profiles.css';
import AccountInformation from '../../../users/candidate-user/components/AccountInformation';

class ListOfCandidates extends Component {
    constructor() {
        super()
        this.state = {
            candidates: [],
            user_info: [],
            data_is_set: false,
            info_is_set:false
        }
    }

    getCandidates = () => {
        fetch('http://inno-admission.live:8000/api/users/?format=json', {
            method: 'GET',
        })
            .then(results => {
                return results.json();
            })
            .then(data => {
                this.setState({
                    candidates: data,
                    data_is_set: true});
                console.log('data: ', data);
            })

    };

    getProfile = (id) => {
        fetch('http://inno-admission.live:8000/api/users/'+id+'/', {
            method: 'GET',
        })
            .then(results => {
                return results.json();
            })
            .then(data => {
                this.setState({
                    user_info: data,
                    info_is_set: true});
                console.log('data: ', data);
            });


    };

    candidatesList = () => {
        return this.state.candidates.map((item, idx) => {
            let accountInformation = null;
            let displayAccountInformation = false;
            if ( displayAccountInformation ) {
                accountInformation = (
                    <div>
                        <AccountInformation
                            username={this.state.user_info.first_name}
                            usersurname={this.state.user_info.last_name}
                            emailAddress={this.state.user_info.email}
                            birthday={this.state.user_info.birth_date}
                        />
                    </div>
                )
            }
            return (
                <div
                    className='list_text'
                >
                    <p>{`${item.id}. `}{item.first_name} {item.last_name}</p>
                    <button className="view_button"   key ={item.id}
                            onClick = {() =>
                            {displayAccountInformation?
                                displayAccountInformation = !displayAccountInformation:
                                this.getProfile(item.id)}}>
                        {displayAccountInformation? "Hide Information" : "View Information"}</button>
                    {accountInformation}
                </div>
            )
        });
    }




    render() {
        if (!this.state.request_done) {
            this.getCandidates()
            this.setState({request_done: true})
        }

        return (
            <div>
                <ProfileHeader username="Nadezhda" usersurname="Zagvozkina"/>

                <h1>Candidates List</h1>
                {
                    this.state.data_is_set ?
                        this.candidatesList()
                        :
                        null
                }
            </div>
        )
    }
}

ListOfCandidates.propTypes = {}
export default ListOfCandidates;