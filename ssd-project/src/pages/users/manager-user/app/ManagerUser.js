import React, { Component } from 'react';
import AccountInformation from '../components/AccountInformation';
import ProfileHeader from '../components/ProfileHeader';
import AvatarComponent from "../components/AvatarComponent";
import TestsAndDocumentation from "../components/TestsAndDocuments";
import { Link } from "react-router-dom";
import authenticate_user from '../../../authenticate_user';

class ManagerUser extends Component {

    handleTestEditorButton = async (event) => {
        event.preventDefault();

        const auth_response = await authenticate_user(this.getCookie('auth_token'));
        if (auth_response['anonymous'] === true) this.props.history.push('/authorization/');
        else if (auth_response['enrollee'] === true) this.props.history.push('/candidate-test/')
        else if (auth_response['staff'] === true) this.props.history.push('/ui-staff-user/')
        else if (auth_response['manager'] === true) this.props.history.push('/test-add-modify/');
        else this.props.history.push('/test-add-modify/');
    }

    render() {
        return (
            <div>
                <ProfileHeader username="Nadezhda" usersurname="Zagvozkina" />
                <AvatarComponent usersurname="Zagvozkina" username="Nadezhda" program="Bachelor" />
                <table align="center">
                    <tr>
                        <div className="profile_box" id="test_box">
                            <a id="test_margin">Add new test or modify existing one</a>
                            <button className="profile_buttons" type="button" onClick={this.handleTestEditorButton}>
                                Test editor
                            </button>
                        </div>
                    </tr>
                </table>
                <div>
                    <table align="center">
                        <tr>
                            <td> <AccountInformation
                                username="Nadezhda"
                                usersurname="Zagvozkina"
                                emailAddress="nadya@whatever.com"
                                birthday="27.01.1998"
                            /></td>
                            <td>
                                <TestsAndDocumentation
                                    isPassport={true}
                                    isCertificate={false}
                                />
                            </td>
                        </tr>
                    </table>
                </div>


            </div>
        )
    }
}

ManagerUser.propTypes = {

}
export default ManagerUser;