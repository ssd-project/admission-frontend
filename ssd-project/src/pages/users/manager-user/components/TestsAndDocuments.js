import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const TestsAndDocumentation = (props) => {
    return (
        <div>
            <div className="profile_box">
                <h2 id="profile_info_header">Candidates test results</h2>
            </div>
            <div className="profile_box">
                <h2 id="profile_info_header">Assigned interviews</h2>
                <Link to="/assign-interview/">
                    <button className="signButtons">Assign interview</button>
                </Link>
            </div>

        </div>
    )
};

TestsAndDocumentation.propTypes = {
    isPassport: PropTypes.bool,
    isCertificate: PropTypes.bool
};

export default TestsAndDocumentation;
