import React from 'react';
import PropTypes from 'prop-types';
import Avatar from 'react-avatar';
const AvatarComponent = (props) => {
    return (
        <div className ="avatar_component">
            <div>
                <Avatar size = "180px" name = {props.username +" "+ props.usersurname} />
            </div>
            <div>
                <h3> {props.username +" "+ props.usersurname}</h3>
            </div>
            <div>
                <h3>Manager</h3>
            </div>
        </div>
    )
};

AvatarComponent.propTypes = {
    username: PropTypes.string.isRequired,
    usersurname:  PropTypes.string.isRequired
};

export default AvatarComponent;
