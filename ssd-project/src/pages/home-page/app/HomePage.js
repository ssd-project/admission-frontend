import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../index.css'
import inno from '../../../inno.jpg';

export default class HomePage extends Component {
    render() {
        return (
            <div className="mainPage">
                <header className="header">
                    <Link to="/authorization">
                        <button  id="signIn" className="headerBtn" >
                            Sign In
                            </button>
                    </Link>
                    <Link to="/registration">
                        <button className="headerBtn" >
                            Sign Up
                        </button>
                    </Link>
                </header>
                <body className="mainImg">
                    <a className="getYour">
                        Get Your Degree in Information Technology
                    </a>
                    <br/><br/>
                    <a className="degree">
                        Bachelor and Master Degree Programs
                    </a>
                    <br/>
                    <a className="open">
                        Open for applications!
                    </a>
                    <br/>
                    <Link to="/registration">
                        <button className="applyBtn" >
                            Apply Now
                        </button>
                    </Link>
                <div >
                <img className="mainI" src={inno} alt="Inno" />
                </div>
                </body>
            </div>
        );
    }
}