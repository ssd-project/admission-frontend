import React from 'react';

const Button = (props) => {
    return (
        <button type="button">
            {props.question}
        </button >
    );
}

export default Button;