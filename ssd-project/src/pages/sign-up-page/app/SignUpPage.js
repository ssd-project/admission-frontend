import React, { Component } from 'react'
import InputForm from '../components/InputForm';
import { Link } from 'react-router-dom';
import { getStudyPrograms, registerUser } from '../api-calls/api-calls';

class SignUpPage extends Component {
    defaultState = {
        firstName: ['firstName', 'First name'],
        lastName: ['lastName', 'Last name'],
        country: ['country', 'Country'],
        city: ['city', 'City'],
        birthday: ['birthday', 'YYYY-MM-DD'],
        program: '',
        email: ['email', 'Email'],
        password: ['password', 'Password'],

        programs: [],
        programRequested: false,
        programRequestCompleted: false
    }

    state = {
        firstName: ['firstName', 'First name'],
        lastName: ['lastName', 'Last name'],
        country: ['country', 'Country'],
        city: ['city', 'City'],
        birthday: ['birthday', 'YYYY-MM-DD'],
        program_id: '',
        email: ['email', 'Email'],
        password: ['password', 'Password'],

        programs: [],
        programRequested: false,
        programRequestCompleted: false
    }

    setProgramsData = (data) => {
        this.setState({
            programRequestCompleted: true,
            programs: data,
            program_id: data[0].id
        })
    }

    onChangeProgram = (event) => {
        this.setState({ program_id: event.target.value[0] });
    }

    onChangeSex = () => {
        this.setState({ sex: this.state.sex === 'Male' ? 'Female' : 'Male' });
    }

    handleInput = (stateValue, inputValue) => {
        let value;
        (inputValue === '') ?
            value = [stateValue, this.defaultState[stateValue][1]]
            :
            value = [stateValue, inputValue];
        this.setState({ [stateValue]: value });
    }

    handleRegisterButton = (event) => {
        let isFieldMissed = false;

        if (this.state.firstName[1] === this.defaultState.firstName[1])
            isFieldMissed = true;
        else if (this.state.lastName[1] === this.defaultState.lastName[1])
            isFieldMissed = true;
        else if (this.state.country[1] === this.defaultState.country[1])
            isFieldMissed = true;
        else if (this.state.birthday[1] === this.defaultState.birthday[1])
            isFieldMissed = true;
        else if (this.state.email[1] === this.defaultState.email[1])
            isFieldMissed = true;
        else if (this.state.password[1] === this.defaultState.password[1])
            isFieldMissed = true;

        if (isFieldMissed) {
            alert("Fill in all fields, please!");
            return;
        }

        const register_response = registerUser(this.state);

        this.props.history.push('/authorization');
    }

    render() {
        if (!this.state.programRequested) {
            getStudyPrograms(this.setProgramsData);
            this.setState({ programRequested: true });
        }
        return (
            <div id="sign" className="mainPage">
                <div>
                    <Link to="/home">
                        <button className="signButtons" type="button">
                            Home
                        </button >
                    </Link>
                </div>
                <div>
                    <div className="signBox">
                        <Link to="/authorization">
                            <button className="signButtons" type="button">
                                Sign In
                        </button >
                        </Link>
                        <Link to="/registration">
                            <button className="signButtons" type="button">
                                Sign Up
                        </button >
                        </Link>
                        <InputForm placeholder={this.state.firstName} handleInput={this.handleInput} />
                        <InputForm placeholder={this.state.lastName} handleInput={this.handleInput} />
                        <InputForm placeholder={this.state.country} handleInput={this.handleInput} />
                        <InputForm placeholder={this.state.city} handleInput={this.handleInput} />
                        <InputForm placeholder={this.state.birthday} handleInput={this.handleInput} />
                        {
                            this.state.programRequestCompleted ?
                                <select id="selectDropdown" onChange={this.onChangeProgram}>
                                    {
                                        this.state.programs.map((item) => { return (<option>{item.id}: {item.name}</option>) })
                                    }
                                </select>
                                :
                                null
                        }
                        <InputForm placeholder={this.state.email} handleInput={this.handleInput} />
                        <InputForm placeholder={this.state.password} handleInput={this.handleInput} type={"password"} />
                        <button className="buttons" type="button" onClick={this.handleRegisterButton}>
                            Register
                        </button>
                    </div>
                </div>
                <div><br /><br /><br /><br /></div>
            </div>
        );
    }

}


export default SignUpPage;