import React from 'react';

const InputForm = (props) => {

    const handleInputChange = (event) => {
        event.preventDefault();
        props.handleInput(props.placeholder[0], event.target.value);
    }

    return (
        <form onChange={handleInputChange}>
            <input  className="textForm" placeholder={props.placeholder[1]} type={props.type} />
        </form>
    );
}

export default InputForm;