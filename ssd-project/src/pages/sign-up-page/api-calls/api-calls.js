
const getStudyPrograms = (setData) => {
    const url = `http://inno-admission.live:8000/api/edu/`;
    fetch(url,
        { method: 'GET' })
        .then((response) => { return response.json(); })
        .then((data) => { setData(data); });
}

const registerUser = async (state) => {
    let registrationData = {};

    const setData = (data) => {
        registrationData = data;
    };
    console.log('state: ', state);
    const url = `http://inno-admission.live:8000/api/users/registration/`;
    await fetch(url,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: state.email[1],
                first_name: state.firstName[1],
                last_name: state.lastName[1],
                birth_date: state.birthday[1],
                country: state.country[1],
                city: state.city[1],
                password: state.password[1],
                edu_program_id: state.program_id
            })
        })
        .then((response) => {
            if (!response.ok) throw new Error(response.status);
            else { return response.json(); }
        })
        .then((data) => {
            setData(data);
            console.log('data: ', data);
        })
        .catch((error) => { console.log("register api Error: ", error); });

    return registrationData;
}

export { getStudyPrograms, registerUser };