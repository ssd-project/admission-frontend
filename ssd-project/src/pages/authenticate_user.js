const authenticate_user = async (token) => {
    let role = '';
    const url = 'http://inno-admission.live:8000/api/users/role/';

    const setRole = (data) => { role = data; }
    await fetch(url, {
        method: 'GET',
        headers: { 'Authorization': `JWT ${token}` }
    })
        .then((response) => { return response.json(); })
        .then((data) => { setRole(data); });
    return role;
}

export default authenticate_user;