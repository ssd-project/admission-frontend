FROM node:latest


ADD ./ssd-project /ssd-project
WORKDIR /ssd-project

RUN npm install
CMD npm start
